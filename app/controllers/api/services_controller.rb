class Api::ServicesController < ApiController

	def index
		
		if params[:q] != nil
				@services = Service.full_text_search(params[:q], allow_empty_search: false).where(active: true)
		elsif params[:organ_id]
			@services = Service.where(organ_id: params[:organ_id]).page(params[:page]).per(60)
		
		elsif params[:letter]
			@services = Service.where(name: /^#{params[:letter]}/i).page(params[:page]).per(60)
		
		elsif params[:active]
			@services = Service.where(active: params[:active])

		else
			@services = Service.where(active: true).page(params[:page]).order(name: :asc)
		  #@services = @ordena.per(60)
			#@serall =Service.where(active: true)
		  #@servicess = @ordena.per(60).order(name: :asc)		    
		    
			@total_pages = @services.total_pages
		end
	end

	def show
		@services = Service.find(params[:id])
	end

end
