class Admin::Organ
  include Mongoid::Document
  include Mongoid::Search
  include Mongoid::Pagination

  mount_uploader :attachment, AttachmentUploader

  field :name, type: String
  field :active, type: String
  field :address, type: String
  field :acronym, type: String
  field :phones, type: String
  field :emails, type: String
  field :image, type: String
  field :permalink, type: String
  field :popular_names, type: Array
  field :schedules, type: Array
  field :nature, type: String
  field :coordinates, type: Hash
  field :date_create, type: String
  field :date_modify, type: String
  field :chief_executive_officer, type: String
  field :office, type: String
  field :official_site, type: String
  field :official_email, type: String
  field :county, type: String

  field :report_url, type: String  


  has_many :services, class_name: "Service"
  has_many :units, class_name: "Admin::Unit"

  before_save :touch_permalink

  search_in :name

  validates_processing_of :attachment
  validate :image_size_validation

  def url
    "/orgao/#{permalink}"
  end

  private
    def image_size_validation
      errors[:attachment] << "Tamanho deve ser menor que 500 Mb" if attachment.size > 0.5.megabytes
    end

  private
    def touch_permalink
      self.permalink = self.name.parameterize
    end
end