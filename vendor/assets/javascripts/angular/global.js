app.run(function($rootScope, ngDialog, Organ, Unit) {
	$rootScope.types_delivery_channels = [
		{ value: 'aplicativo-movel', label: 'Aplicativo Móvel' },
		{ value: 'e-mail', label: 'E-mail' },
		{ value: 'fax', label: 'Fax' },
		{ value: 'postal', label: 'Postal' },
		{ value: 'presencial', label: 'Presencial' },
		{ value: 'sms', label: 'SMS' },
		{ value: 'telefone', label: 'Telefone' },
		{ value: 'web', label: 'Web' },
		// { value: 'web-acompanhar', label: '???' },
		// { value: 'web-agendar', label: '???' },
		// { value: 'web-calcular-taxas', label: '???' },
		// { value: 'web-consultar', label: '???' },
		// { value: 'web-declarar', label: '???' },
		// { value: 'web-emitir', label: '???' },
		// { value: 'web-inscrever-se', label: '???' },
		// { value: 'web-postos-de-atendimento', label: '???' },
		// { value: 'web-preencher', label: '???' },
		// { value: 'web-simular', label: '???' }
	];

	$rootScope.m_units = [
		{ value: 'minutos', label: 'Minutos' },
		{ value: 'horas', label: 'Horas' },
		{ value: 'dias', label: 'Dias' },
		{ value: 'dias-corridos', label: 'Dias corridos' },
		{ value: 'dias-uteis', label: 'Dias úteis' },
		{ value: 'meses', label: 'Meses' }
	];

	$rootScope.addOrgan = function () {
    ngDialog.open({ template: 'organForm' });
  };

	$rootScope.saveOrgan = function (name) {
    $rootScope.organs.push({ value: 321, label: name });
    ngDialog.close();
  };
});
