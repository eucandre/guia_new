  class Admin::OrgansController < AdminController
  before_action :set_admin_organ, only: [:show, :edit, :update, :destroy]

  before_action :verify_permission, if: -> { request.path_parameters[:action] == 'index' && !request.format.json? }

  skip_before_filter :verify_authenticity_token

  def index
    @admin_organs = Admin::Organ.all
  end

  def new
    @admin_organ = Admin::Organ.new
  end

  def show
    render json: @admin_organ
  end

  def edit
    
  end

  def uploader
    organ = Admin::Organ.find(params[:organ_id])
    organ.attachment = params[:file]

    render json: organ.save

  end

  def create
    @admin_organ = Admin::Organ.new(admin_organ_params)

    respond_to do |format|
      if @admin_organ.save
        format.html { redirect_to admin_organs_url, notice: 'Órgão criado com sucesso.' }
        format.json { render :show, status: :created, location: @admin_organ }
      else
        format.html { render :new }
        format.json { render json: @admin_organ.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      
      if @admin_organ.update(admin_organ_params)
        format.html { redirect_to admin_organs_url, notice: 'Órgão atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @admin_organ }
      else
        format.html { render :edit }
        format.json { render json: @admin_organ.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_organ.destroy
    respond_to do |format|
      format.html { redirect_to admin_organs_url, notice: 'Órgão excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_organ
       @admin_organ = Admin::Organ.find(params[:id])
    end

    def admin_organ_params
      params.permit(
                      :name,
                      :active,
                      :address,
                      :acronym,
                      :phones,
                      :emails,
                      :image,
                      :nature,
                      :attachment,
                      :report_url,
                      :chief_executive_officer,
                      :office,
                      :official_email,
                      :official_site,
                      :county,
                      :report_url,
                      popular_names: [:name],
                      schedules: [:day, :start, :end],
                      coordinates: [:latitude, :longitude],
                    )
    end
end
