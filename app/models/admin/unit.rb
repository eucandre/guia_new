class Admin::Unit
  include Mongoid::Document
  include Mongoid::Search

  field :name, type: String
  field :active, type: Boolean
  field :address, type: String
  field :phones, type: String
  field :email, type: String
  field :image, type: String
  field :permalink, type: String
  field :schedules, type: Array
  field :note_schedules, type: String
  field :coordinates, type: Hash
  field :county, type: String
  field :date, type: String
  field :date_modified, type: String
  
  belongs_to :organ, class_name: "Admin::Organ"
  belongs_to :user, class_name: "User"
  has_and_belongs_to_many :services, class_name: "Service"

  paginates_per 8

  before_save :touch_permalink

  search_in :name

  def organ_id
    read_attribute(:organ_id).to_s
  end

  def service_id
    read_attribute(:service_id).to_s
  end

  def user_id
    read_attribute(:user_id).to_s
  end

  def url
    "/unidade-de-atendimento/#{permalink}"
  end

  private
    def touch_permalink
      self.permalink = self.name.parameterize
    end
end
