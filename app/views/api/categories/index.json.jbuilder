json.array!(@categories) do |category|
  json.id category.id.to_s
  json.name category.name
  json.icon category.icon
  json.active category.active
end
