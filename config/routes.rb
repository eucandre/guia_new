Rails.application.routes.draw do

  devise_for :users

  root 'home#index'

  get 'guia-de-servicos', to: 'services#index'
  get 'servico/:id/:permalink', to: 'services#show'
  get 'categoria/:category_slug/servicos', to: 'categories#index'
  get 'publico_alvo/:audience_slug/servicos', to: 'audiences#index'

  get 'orgaos', to: 'organs#index'
  get 'orgao/:permalink', to: 'organs#show'

  get 'unidades-de-atendimento', to: 'units#index'
  get 'unidade-de-atendimento/:permalink', to: 'units#show'

  get 'central-de-atendimento', to: 'centers#index'
  get 'central-de-atendimento/:permalink', to: 'centers#show'

  get 'categoria/:permalink', to: 'categories#index'
  get 'publico-alvo/:permalink', to: 'audiences#index'

  get 'arquivos', to: 'home#arquives'
  
  get 'api', to: 'home#api'

  get 'sobre', to: 'home#about'

  namespace :api do
    resources :services, :organs, :units, :centers, :categories, :audiences
    namespace :v1 do
      resources :services, :organs, :units, :centers, :categories, :audiences
    end
  end

  get 'admin', to: redirect('/admin/dashboard')

  namespace :admin do
    resources :services, :organs, :units, :centers, :categories, :audiences, :profiles, :users
    get 'dashboard', to: 'dashboard#index'
    get 'auth_generator', to: 'dashboard#auth_generator'
    post 'organs/uploader', to: 'organs#uploader'
  end

end
