json.services @services.each do |service|
  json.id service.id.to_s
  json.name service.name
  json.active service.active
  json.highlights service.highlights
  json.highlights_value service.highlights_value
  json.date service.date
  json.date_modified service.date_modified
  json.free service.free
  json.estimated_time service.estimated_time
  json.acronym service.acronym
  json.description service.description
  json.permalink service.permalink
  json.url service.url
  json.steps service.steps
  json.mantener service.mantener
  json.coordinator service.coordinator
  json.maturity_level service.maturity_level
  json.latest_updater service.latest_updater
  json.other_informations service.other_informations
  json.organ do 
    json.id service.organ.id.to_s
    json.name service.organ.name
    json.address service.organ.address
    json.acronym service.organ.acronym
    json.phones service.organ.phones
    json.emails service.organ.emails
    json.schedules service.organ.schedules
    json.image service.organ.image
    json.permalink service.organ.permalink
  end if service.organ
  json.categories service.categories do |category|
    json.id category.id.to_s
    json.name category.name
    json.permalink category.permalink
    json.active category.active
    json.icon category.icon
  end
  json.units service.units do |unit|
    json.id unit.id.to_s
    json.name unit.name
    json.address unit.address
    json.county unit.county
  end
end

json.total_pages @total_pages
json.serall @serall
json.servicess @servicess
json.tam_letter @tam_letter