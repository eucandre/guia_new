json.array!(@services) do |service|
  json.id service.id.to_s
  json.name service.name
  json.active service.active
  json.description service.description
  json.popular_names service.popular_names
  json.maturity_level service.maturity_level
  json.url service.url
  json.latest_updater service.latest_updater
  json.date service.date
  json.organ service.organ_id.to_s
end
