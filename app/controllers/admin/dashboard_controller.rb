class Admin::DashboardController < AdminController

	def index
		@user = User.all
		@todos = Service.all
		@services = Service.where(active: true)
		@highligth = Service.where(highlights: true)
		@freeler = Service.where(free: true)
		@not_freeler = Service.where(free: false)
	end

	def auth_generator
		AuthController.delete_all
		AuthAction.delete_all

		actions = {
		  index: 'Listar',
		  new: 'Tela de criação',
		  create: 'Criar',
		  show: 'Visualização',
		  edit: 'Tela de edição',
		  update: 'Atualizar',
		  destroy: 'Remover',
			uploader: 'Upload'
		}

		Rails.application.eager_load!

		AdminController.subclasses.each do |controller|
			if controller != Admin::DashboardController
		    controller_object = if AuthController.where(name: controller).first
		      AuthController.where(name: controller).first
		    else
		      AuthController.create(name: controller)
		    end

		    controller.action_methods.each do |action|
		      if action != 'verify_permission' && action != 'cached'
		        AuthAction.create(name: action, description: actions[action.to_sym], auth_controller_id: controller_object.id) if AuthAction.where(name: action, description: actions[action.to_sym], auth_controller_id: controller_object.id).first.nil?
		      end
		    end
			end
		end

		render text: 'ok'
	end

end
