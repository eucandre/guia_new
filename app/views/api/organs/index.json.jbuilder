json.array!(@organs) do |organ|
  json.id organ.id.to_s
  json.name organ.name
  json.attachment organ.attachment
  json.report_url organ.report_url
  json.active organ.active
  json.address organ.address
  json.acronym organ.acronym  
  json.phones organ.phones
  json.emails organ.emails
  json.county organ.county
  json.popular_names organ.popular_names
  json.schedules organ.schedules
  json.image organ.image
  json.nature organ.nature
  json.permalink organ.permalink
  json.url organ.url
  json.services organ.services
  json.units organ.units
  json.office organ.office
  json.chief_executive_officer organ.chief_executive_officer
end
