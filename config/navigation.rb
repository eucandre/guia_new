SimpleNavigation::Builder.config do |map|
  map.navigation :default do |navigation|

    # Root menu without child elements (menus) that points to /dashboard
    navigation.menu :home, :url => { :controller => "home", :action => "index"}

    # Root menu with child menus without anchor link
    navigation.menu :contacts do |contacts|

      # Child menu with many possible urls (or many controllers and actions)
      contacts.menu :list, :url => { :controller => "contacts", :action => "index" } do |contact_list|

        # This menu will marked as current when you're on the following
        # controllers and actions (including the controller and action
        # specified in the :url option):
        contact_list.connect :controller => "contacts" # ...current on any action from this controller
        contact_list.connect :controller => "people", :except => "new"
        contact_list.connect :controller => "companies", :except => "new"

      end

      # Another submenu that points to /person/new
      contacts.menu :new_person, :url => { :controller => "people", :action => "new" }

      # Another submenu that points to /company/new
      contacts.menu :new_company, :url => { :controller => "companies", :action => "new" }

    end

    # Another root menu with nested submenus
    navigation.tab :admin, :url => { :controller => "users", :action => "index" } do |admin|
      admin.menu :users, :url => { :controller => "users", :action => "index" } do |users|
        users.menu :reports, :url => { :controller => "user_reports", :action => "index" } do |reports|
          reports.menu :activity, :url => { :controller => "user_reports", :action => "activity" }
          reports.menu :login_atempts, :url => { :controller => "user_reports", :action => "login_atempts" }
        end
        users.menu :new_user, :url => { :controller => "users", :action => "new" }
      end
    end

  end
end
