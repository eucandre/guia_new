class	UnitsController < ApplicationController

	def index
		@units = Admin::Unit.where(active: params[:true]).all
		@organs = Admin::Organ.where(active: true).order(name: :asc)
		@u = Admin::Unit.all
	end

	def show
		@unit = Admin::Unit.where(permalink: params[:permalink]).first
		@service_activ = @unit.services.where(active: true).length

	end

end
