require 'test_helper'

class Admin::OrgansControllerTest < ActionController::TestCase
  setup do
    @admin_organ = admin_organs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_organs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_organ" do
    assert_difference('Admin::Organ.count') do
      post :create, admin_organ: { acronym: @admin_organ.acronym, address: @admin_organ.address, emails: @admin_organ.emails, geo: @admin_organ.geo, image: @admin_organ.image, name: @admin_organ.name, phones: @admin_organ.phones, schedules: @admin_organ.schedules }
    end

    assert_redirected_to admin_organ_path(assigns(:admin_organ))
  end

  test "should show admin_organ" do
    get :show, id: @admin_organ
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_organ
    assert_response :success
  end

  test "should update admin_organ" do
    patch :update, id: @admin_organ, admin_organ: { acronym: @admin_organ.acronym, address: @admin_organ.address, emails: @admin_organ.emails, geo: @admin_organ.geo, image: @admin_organ.image, name: @admin_organ.name, phones: @admin_organ.phones, schedules: @admin_organ.schedules }
    assert_redirected_to admin_organ_path(assigns(:admin_organ))
  end

  test "should destroy admin_organ" do
    assert_difference('Admin::Organ.count', -1) do
      delete :destroy, id: @admin_organ
    end

    assert_redirected_to admin_organs_path
  end
end
