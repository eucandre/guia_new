var app = angular.module('App', ['cgBusy']);

app.filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i=1; i<=total; i++) {
      input.push(i);
    }

    return input;
  };
});

app.controller('HomeIndexController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

  $scope.page = 1;
  $scope.limit = 100;

  $scope.servicesPromise =
    $http
      .get('/api/services.json')
      .success(function (data) {
        $scope.total_pages = data.total_pages;
        $scope.services = data.services;
        $scope.tam = data.serall;
        resetItems();
      });

  $scope.numberPage = function (number){
    $scope.servicesPromise = $scope.page =
      $http
        .get('/api/services.json?page=' + $scope.page)
        .success(function (data) {
          $scope.total_pages = data.total_pages;
          $scope.services = data.services;
          resetItems();
        }).length;
  }

  $scope.$watch('q', function (newValue, oldValue) {
      $scope.servicesPromise =
        $http
          .get('/api/services.json?q=' + newValue)
          .success(function (data) {
            $scope.total_pages = data.total_pages;
            $scope.services = data.services;
            resetItems();
          });
    }, true);

  var resetItems = function () {
    $timeout(function () {
      var heights = [];
      $('body .service-item').each(function (index) {
        heights.push($(this)[0].clientHeight);
      });
      var largest = Math.max.apply(Math, heights);
      $('body .service-item').css({height: largest});
    }, 1);
  };
}]);

app.controller('UnitsIndexController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

  $scope.page = 1;
  $scope.limit = 100;

  $scope.unitsPromise =
    $http
      .get('/api/units.json')
      .success(function (data) {
        $scope.total_pages = data.total_pages;
        $scope.units = data.units;
        $scope.t_unidades = data.t_units;

      });

  $scope.alphabet = "abcdefghijklmnopqrstuvwxyz1234567890".split("");

  $scope.byLetter = function (letter) {
    $scope.unitsPromise =
      $http
        .get('/api/units.json?letter=' + letter)
        .success(function (data) {
          $scope.total_pages = data.total_pages;
          $scope.units = data.units;
        });
  };

  
  $scope.$watch('q', function (newValue, oldValue) {
    if (oldValue != null) {
      $scope.unitsPromise =
        $http.get('/api/units.json?q=' + newValue)
              .success(function (data) {
                $scope.total_pages = data.total_pages;
                $scope.units = data.units;
              });
    };
  }, true);

  $scope.loadPaginate = function (direction, page) {
    if (direction != null) {
      if (direction == 0 && $scope.page > 1) {
        $scope.page = $scope.page - 1;
      }
      if (direction == 1 && $scope.page < 100) {
        $scope.page = $scope.page + 1;
      }
    }

    if (page != null) {
      $scope.page = page;
    }

    $scope.unitsPromise =
      $http
        .get('/api/units.json?page=' + $scope.page)
        .success(function (data) {
          $scope.total_pages = data.total_pages;
          $scope.units = data.units;
          $scope.t_und = data.t_und;
          
        });
  }

  $scope.$watch('organ_id', function (newValue, oldValue) {
    if (newValue != null && newValue != '') {
      $scope.unitsPromise =
        $http
          .get('/api/units.json?organ_id=' + newValue)
          .success(function (data) {
            $scope.units = data.units;
            
          });
    };
  }, true);
}]);

app.controller('ServicesIndexController', ['$scope', '$http', '$timeout',function ($scope, $http, timeout) {
  $scope.page = 1;
  $scope.prox = 1;

  $scope.servicesPromise =
    $http
      .get('/api/services.json')
      .success(function (data) {
        //$scope.total_pages = data.total_pages;
        $scope.servicess = data.servicess;
        $scope.services = data.services;
        $scope.tam= data.serall;
      });
    
  $scope.alphabet = "abcdefghijklmnopqrstuvwxyz1234567890".split("");
  

  $scope.byLetter = function (letter, organ_id) {
        if (organ_id==null ){
          $scope.servicesPromise =
            $http
              .get('/api/services.json?letter=' + letter)
              .success(function (data) {
                $scope.total_pages = data.total_pages;
                $scope.services = data.services;
                $scope.tama_letra = data.tam_letter;

              });
        }
        if (organ_id != null){
          
          $scope.servicesPromise =
            $http
              .get('/api/services.json?organ_id=' + organ_id)
              .success(function (data) {
                  $scope.total_pages = data.total_pages;
                  $scope.services = data.services;
              });  

        }

  };

  $scope.filter_search = function (organ_id, letter) {
    var params = {};

    params.organ_id = organ_id;
    params.letter = letter;

    $scope.servicesPromise = $http.get('/api/services.json?params=', {params: params})
    .success(function(data) {
      $scope.total_pages = data.total_pages;
      $scope.services = data.services;
    });
  };
  
  $scope.$watch('q', function (newValue, oldValue) {
    if (oldValue != null ) {
      $scope.servicesPromise =
        $http
          .get('/api/services.json?q=' + newValue)
          .success(function (data) {
            $scope.total_pages = data.total_pages;
             $scope.services = data.services;
            for (i=0;i<=data.services.length;i++){
              if ($scope.services[i].active==true){
                $scope.servicess = data.services[i];
                $scope.tama = $scope.servicess.length;
              }
            }
          });
    };
  }, true);

  $scope.$watch('organ_id', function (newValue, oldValue) {
      if (newValue != null && newValue != '') {
        $scope.servicesPromise =
          $http
            .get('/api/services.json?organ_id=' + newValue)
            .success(function (data) {
              $scope.total_pages = data.total_pages;
              $scope.services = data.services;
            });
      };
    }, true);

  $scope.loadPaginate = function (direction, page) {
    if (direction != null) {
      if (direction == 0 && $scope.page > 1) {
        $scope.page = $scope.page - 1;
      }
      if (direction == 1 && $scope.page < 100) {
        $scope.page = $scope.page + 1;
      }
    }

    if (page != null) {
      $scope.page = page;
    }

    $scope.servicesPromise =
      $http
        .get('/api/services.json?page=' + $scope.page)
        .success(function (data) {
          $scope.total_pages = data.total_pages;
          $scope.services = data.services;
          resetItems();
        });
  }
}]);

app.controller('OrgansIndexController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

  $scope.choices = [
    {name: "Crescente"},
    {name: "Decrescente"}
  ];

  $scope.organsPromise =
    $http
      .get('/api/organs.json')
      .success(function (data) {
        $scope.organs = data;
        resetItems();
      });

  $scope.loadPaginate = function (direction) {
    if (direction == 0 && $scope.page > 1) {
      $scope.page = $scope.page - 1;
    }

    if (direction == 1 && $scope.page < 100) {
      if ($http.get('/api/organs.json?page=' + $scope.page).length==null) {
        $scope.page = $scope.page*1;
      }
      else{
        $scope.page = $scope.page+1;
      }
    }

    $scope.organsPromise =
      $http
        .get('/api/organs.json?page=' + $scope.page)
        .success(function (data) {
          $scope.services = data;
          resetItems();
        });
  };

  $scope.alphabet = "abcdefghijklmnopqrstuvwxyz0123456789".split("");

  $scope.byLetter = function (letter) {
    $scope.organsPromise =
      $http
        .get('/api/organs.json?letter=' + letter)
        .success(function (data) {
          $scope.organs = data;
          resetItems();
        });
  };


  $scope.$watch('q', function (newValue, oldValue) {
    if (oldValue != null) {
      $scope.organsPromise =
        $http
          .get('/api/organs.json?q=' + newValue)
          .success(function (data) {
            $scope.organs = data;
            resetItems();
          });
    };
  }, true);

  $scope.sortOrgan = function (order) {
    order = 1;
    if (order == 1){
      $scope.organsPromise =
        $http
          .get('/api/organs.json').success(function (data) {
            $scope.organs = data;
            resetItems();
          }).sort();
    }
    if (order == 2){
      $scope.organsPromise =
        $http
          .get('/api/organs.json')
          .success(function (data) {
            $scope.organs = data;
            resetItems();
          }).sort().reverse();
    }
  };

  var resetItems = function () {
    $timeout(function () {
      var heights = [];
      $('body .service-item').each(function (index) {
        heights.push($(this)[0].clientHeight);
      });
      var largest = Math.max.apply(Math, heights);
      $('body .service-item').css({height: largest});
    }, 200);
  };
}]);

app.controller('CentersIndexController', ['$scope', '$http', function ($scope, $http) {
  $http.get('/api/centers.json')
        .success(function (data) {
          $scope.centers = data;
        });

  $scope.alphabet = "abcdefghijklmnopqrstuvwxyz".split("");

  $scope.byLetter = function (letter) {
    $http.get('/api/centers.json?letter=' + letter)
          .success(function (data) {
            $scope.centers = data;
          });
  };

  $scope.$watch('q', function (newValue, oldValue) {
    if (oldValue != null) {
      $http.get('/api/centers.json?q=' + newValue)
            .success(function (data) {
              $scope.centers = data;
            });
    };
  }, true);

  $scope.$watch('organ_id', function (newValue, oldValue) {
    if (newValue != null && newValue != '') {
      $http.get('/api/centers.json?organ_id=' + newValue)
            .success(function (data) {
              $scope.centers = data;
            });
    };
  }, true);
}]);