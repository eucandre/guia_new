json.array!(@admin_audiences) do |admin_audience|
  json.id admin_audience._id.to_s
  json.name admin_audience.name
  json.active admin_audience.active
end
