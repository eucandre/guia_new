app.controller('ProfilesIndexController', ['$scope', '$http', 'Profile', function($scope, $http, Profile) {
	$scope.profiles = Profile.query();

	// $scope.remove = function(organ) {
	// 	if (confirm('Tem certeza que deseja remover o orgão?')) {
	// 		var index = $scope.organs.indexOf(organ);
	// 		$scope.organs.splice(index, 1);
	// 		Organ.delete({ id: organ.id });
	// 	}
	// };
}]);

app.controller('ProfilesNewController', ['$scope', '$http', '$routeParams', '$location', 'Profile', function($scope, $http, $routeParams, $location, Profile) {
	$scope.profile = {};

	$scope.save = function() {
		Profile.save($scope.profile);
		return $location.path('/profiles');
	};
}]);

app.controller('ProfilesEditController', ['$scope', '$http', '$routeParams', '$location', 'Profile', function($scope, $http, $routeParams, $location, Profile) {
	$scope.profile = Audience.get({id: $routeParams.id});

	$scope.save = function() {
		Audience.update($scope.audience);
		return $location.path('/audiences');
	};
}]);
