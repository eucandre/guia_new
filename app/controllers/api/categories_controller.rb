class Api::CategoriesController < ApiController

	def index
		if params[:q]
			@categories =  Admin::Category.full_text_search(params[:q], allow_empty_search: true)
		elsif params[:letter]
			@categories =  Admin::Category.where(name: /^#{params[:letter]}/i)
		else
			@categories =  Admin::Category.order(name: :asc)
		end
	end

	def show
		@category =  Admin::Category.find(params[:id])
	end

end
