//= require jquery
//= require jquery_ujs

//= require lodash/lodash
//= require angular-simple-logger/dist/angular-simple-logger
//= require angular-google-maps/dist/angular-google-maps
//= require vendor/angular-ckeditor
//= require vendor/checklist-model
//= require angular-file-upload/dist/angular-file-upload

//= require angular/app
//= require angular/routes
//= require angular/global
//= require angular/services

//= require angular/controllers/services
//= require angular/controllers/organs
//= require angular/controllers/categories
//= require angular/controllers/units
//= require angular/controllers/audiences
//= require angular/controllers/centers
