if params[:list_units] == false
	json.id @unit.id.to_s
  json.name @unit.name
  json.active @unit.active
  json.address @unit.address
  json.county @unit.county
  json.phones @unit.phones
  json.email @unit.email
  json.schedules @unit.schedules
  json.note_schedules @unit.note_schedules
  json.image @unit.image
  json.permalink @unit.permalink
  json.url @unit.url
  json.organ @unit.organ.name	
else
  json.name @unit.name
	json.services @unit.services 
end
