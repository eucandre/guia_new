class User
  include Mongoid::Document
  include Mongoid::Search

  mount_uploader :attachment, AttachmentUploader

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  ## Database authenticatable
  field :name,               type: String, default: ""
  field :email,              type: String, default: ""
  field :cpf,                type: String, default: ""
  field :encrypted_password, type: String, default: ""

  belongs_to :profile, class_name: "Admin::Profile"
  belongs_to :organ, class_name: "Admin::Organ"

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  validates_uniqueness_of :cpf

  validates_processing_of :attachment
  validate :image_size_validation

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  
  def admin?
    (profile.name == 'Administrador Geral') ? true : false
  end

  def mantenedor?
    (profile.name == 'Mantenedor') ? true : false
  end

  private
    def image_size_validation
      errors[:attachment] << "Tamanho deve ser menor que 500 Mb" if attachment.size > 0.5.megabytes
    end

end
