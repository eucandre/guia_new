class AuthController
  include Mongoid::Document
  field :name, type: String
  field :alias, type: String
  has_many	:auth_actions
end
