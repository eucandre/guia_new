class Admin::ProfilesController < AdminController
  before_action :set_admin_profile, only: [:show, :edit, :update, :destroy]

  before_action :verify_permission

  def index
    @admin_profiles = Admin::Profile.all
  end

  def new
    @admin_profile = Admin::Profile.new
    @controlers = AuthController.all
  end

  def edit
    @admin_profiles = Admin::Profile.page(params[:page])
    @controlers = AuthController.all
  end

  def show
    render json: @admin_profile
  end

  def create
    @admin_profile = Admin::Profile.new(admin_profile_params)

    respond_to do |format|
      if @admin_profile.save
        format.html { redirect_to admin_profiles_url, notice: 'Perfil criado com sucesso.' }
        format.json { render :show, status: :created, location: @admin_profile }
      else
        format.html { render :new }
        format.json { render json: @admin_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_profile.update(admin_profile_params)
        format.html { redirect_to admin_profiles_url, notice: 'Perfil atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @admin_profile }
      else
        format.html { render :edit }
        format.json { render json: @admin_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_profile.destroy

    respond_to do |format|
      format.html { redirect_to admin_profiles_url, notice: 'Perfil Alvo excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_profile
      @admin_profile = Admin::Profile.find(params[:id])
    end

    def admin_profile_params
      params.require(:admin_profile).permit(:name, auth_action_ids: [])
    end
end
