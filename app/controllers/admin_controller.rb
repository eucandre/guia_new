class AdminController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  layout 'admin'

  def verify_permission    
    x = current_user.profile.auth_actions.collect { |a| { controller: a.auth_controller.name, action: a.name } }

    by_pass = false

    x.each do |y|
      if y[:controller] == self.class.to_s && y[:action] == self.action_name
        by_pass = true
        break
      end
    end

    redirect_to :root unless by_pass
  end
end
