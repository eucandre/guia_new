app.controller('AudiencesIndexController', ['$scope', '$http', 'Audience', function($scope, $http, Audience) {
	$scope.audiences = Audience.query();

	// $scope.remove = function(organ) {
	// 	if (confirm('Tem certeza que deseja remover o orgão?')) {
	// 		var index = $scope.organs.indexOf(organ);
	// 		$scope.organs.splice(index, 1);
	// 		Organ.delete({ id: organ.id });
	// 	}
	// };
}]);

app.controller('AudiencesNewController', ['$scope', '$http', '$routeParams', '$location', 'Audience', function($scope, $http, $routeParams, $location, Audience) {
	$scope.audience = {};

	$scope.save = function() {
		Audience.save($scope.audience);
		return $location.path('/audiences');
	};
}]);

app.controller('AudiencesEditController', ['$scope', '$http', '$routeParams', '$location', 'Audience', function($scope, $http, $routeParams, $location, Audience) {
	$scope.audience = Audience.get({id: $routeParams.id});

	$scope.save = function() {
		Audience.update($scope.audience);
		return $location.path('/audiences');
	};
}]);
