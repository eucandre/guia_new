class Admin::AudiencesController < AdminController
  before_action :set_admin_audience, only: [:show, :edit, :update, :destroy]

  before_action :verify_permission, if: -> { request.path_parameters[:action] == 'index' && !request.format.json? }

  def index
    @admin_audiences = Admin::Audience.page(params[:page])
  end

  def new
    @admin_audience = Admin::Audience.new
  end

  def edit
    @audiences = Admin::Audience.page(params[:page])
  end

  def show
    render json: @admin_audience
  end

  def create
    @admin_audience = Admin::Audience.new(admin_audience_params)

    respond_to do |format|
      if @admin_audience.save
        format.html { redirect_to admin_audiences_url, notice: 'Público Alvo criado com sucesso.' }
        format.json { render :show, status: :created, location: @admin_audience }
      else
        format.html { render :new }
        format.json { render json: @admin_audience.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_audience.update(admin_audience_params)
        format.html { redirect_to admin_audiences_url, notice: 'Público Alvo atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @admin_audience }
      else
        format.html { render :edit }
        format.json { render json: @admin_audience.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_audience.destroy
    respond_to do |format|
      format.html { redirect_to admin_audiences_url, notice: 'Público Alvo excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_audience
      @admin_audience = Admin::Audience.find(params[:id])
    end

    def admin_audience_params
      params.require(:admin_audience).permit(:name, :active, :icon)
    end
end
