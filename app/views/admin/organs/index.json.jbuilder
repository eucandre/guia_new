json.array!(@admin_organs) do |admin_organ|
  json.id            admin_organ.id.to_s
  json.name          admin_organ.name
  json.active        admin_organ.active
  json.address       admin_organ.address
  json.county        admin_organ.county
  json.acronym       admin_organ.acronym
  json.phones        admin_organ.phones
  json.nature        admin_organ.nature
  json.emails        admin_organ.emails
  json.schedules     admin_organ.schedules
  json.popular_names admin_organ.popular_names
  json.image         admin_organ.image

end
