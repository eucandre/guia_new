class Api::OrgansController < ApiController

	def index
		if params[:q]
			@organs = Admin::Organ.full_text_search(params[:q], allow_empty_search: true).where(active: true)
		elsif params[:letter]
			@organs = Admin::Organ.where(name: /^#{params[:letter]}/i).where(active: true)
		else
			@organs = Admin::Organ.where(active: true)
		end
	end

	def show
		@organ = Admin::Organ.find(params[:id])
	end

end
