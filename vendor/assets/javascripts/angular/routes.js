// app.config(['$routeProvider',
// 	function($routeProvider) {
// 	  $routeProvider
// 	  .when('/', {
// 	    templateUrl: '/assets/angular/views/dashboard/index.html'
// 	  })
// 	  .when('/services', {
// 	    templateUrl: '/assets/angular/views/services/index.html',
// 	    controller: 'ServicesIndexController'
// 	  })
// 	  .when('/services/new', {
// 	    templateUrl: '/assets/angular/views/services/form.html',
// 	    controller: 'ServicesNewController'
// 	  })		  
// 	  .when('/services/:id/edit', {
// 	    templateUrl: '/assets/angular/views/services/form.html',
// 	    controller: 'ServicesEditController'
// 	  })
// 	  .when('/organs', {
// 	    templateUrl: '/assets/angular/views/organs/index.html',
// 	    controller: 'OrgansIndexController'
// 	  })
// 	  .when('/organs/new', {
// 	    templateUrl: '/assets/angular/views/organs/form.html',
// 	    controller: 'OrgansNewController'
// 	  })		  
// 	  .when('/organs/:id/edit', {
// 	    templateUrl: '/assets/angular/views/organs/form.html',
// 	    controller: 'OrgansEditController'
// 	  })
// 	  .when('/categories', {
// 	    templateUrl: '/assets/angular/views/categories/index.html',
// 	    controller: 'CategoriesIndexController'
// 	  })
// 	  .when('/categories/new', {
// 	    templateUrl: '/assets/angular/views/categories/form.html',
// 	    controller: 'CategoriesNewController'
// 	  })		  
// 	  .when('/categories/:id/edit', {
// 	    templateUrl: '/assets/angular/views/categories/form.html',
// 	    controller: 'CategoriesEditController'
// 	  })
// 	  .when('/audiences', {
// 	    templateUrl: '/assets/angular/views/audiences/index.html',
// 	    controller: 'AudiencesIndexController'
// 	  })
// 	  .when('/audiences/new', {
// 	    templateUrl: '/assets/angular/views/audiences/form.html',
// 	    controller: 'AudiencesNewController'
// 	  })
// 	  .when('/audiences/:id/edit', {
// 	    templateUrl: '/assets/angular/views/audiences/form.html',
// 	    controller: 'AudiencesEditController'
// 	  })
// 	  .when('/units', {
// 	    templateUrl: '/assets/angular/views/units/index.html',
// 	    controller: 'UnitsIndexController'
// 	  })
// 	  .when('/units/new', {
// 	    templateUrl: '/assets/angular/views/units/form.html',
// 	    controller: 'UnitsNewController'
// 	  })		  
// 	  .when('/units/:id/edit', {
// 	    templateUrl: '/assets/angular/views/units/form.html',
// 	    controller: 'UnitsEditController'
// 	  })
// 	  .otherwise({
//       redirectTo: '/'
//     });
// 	}
// ]);
