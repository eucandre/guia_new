json.array!(@admin_units) do |admin_unit|
  json.id admin_unit._id.to_s
  json.name admin_unit.name
  json.active admin_unit.active
  json.user admin_unit.user
  json.address admin_unit.address
  json.phones admin_unit.phones
  json.email admin_unit.email
  json.schedules admin_unit.schedules
  json.image admin_unit.image
  json.permalink admin_unit.permalink
  json.organ admin_unit.organ
  json.url admin_unit.url
  json.county admin_unit.county
end