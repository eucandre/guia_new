if @show_units == 'true'
	json.units @center.units do |unit|
		json.id unit.id.to_s
		json.name unit.name
		json.address unit.address
		json.coordinates unit.coordinates	
		json.schedules unit.schedules
		json.phones unit.phones
		json.email unit.email
		json.organ_id unit.organ.id.to_s
	end 	
elsif	@show_services == 'true' 
	json.service_units @center.units do |service_unit|
		json.service service_unit.services do |i|
			json.service_id i.id.to_s
			json.name i.name
			json.description i.description
			json.categories i.categories do |categori|
				json.categories_id categori.id.to_s
				json.name categori.name
			end
			json.organ_id i.organ.id.to_s
		end
	end
else
	json.id @center._id.to_s
	json.name @center.name
	json.address @center.address
	json.phones @center.phones
	json.email @center.email
	json.coordinates @center.coordinates
	json.schedules @center.schedules
	json.permalink @center.permalink
	json.url @center.url
	json.units @center.units do |unit|
	  json.unit_id unit.id.to_s
		json.organ_id unit.organ_id.to_s
	end
	json.services_units @center.units do |service_uni|
		json.unit service_uni.name
		json.service service_uni.services do |i|
			json.services_id i.id.to_s
			json.name i.name
			json.organ i.organ.id.to_s
		end
end

end

