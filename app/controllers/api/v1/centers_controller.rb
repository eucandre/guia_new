class Api::V1::CentersController < ApiController
	def index
		if params[:q]
			@centers = Admin::Center.full_text_search(params[:q], allow_empty_search: true)
		elsif params[:organ_id]
			@centers = Admin::Center.where(unit_id: params[:unit_id])
		elsif params[:letter]
			@centers = Admin::Center.where(name: /^#{params[:letter]}/i)
		else
			@centers = Admin::Center.order(name: :asc)
		end	
	end
	def show
		@center = Admin::Center.find(params[:id])
		@show_services = params[:show_services]
		@show_units = params[:show_units]
	end	
end