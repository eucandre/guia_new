module Mongoid
  module Document
    def as_json(options={})
      attrs = super(options)
      attrs["id"] = attrs["_id"].to_s
      attrs["unit_ids"] = attrs["unit_ids"].map { |u| u.to_s } if attrs.include? "unit_ids"
      attrs["category_ids"] = attrs["category_ids"].map { |u| u.to_s } if attrs.include? "category_ids"
      attrs["audience_ids"] = attrs["audience_ids"].map { |u| u.to_s } if attrs.include? "audience_ids"
      attrs
    end
  end
end