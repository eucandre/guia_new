json.array!(@users) do |user|
  json.id      user._id.to_s
  json.name    user.name
  json.email   user.email
  json.cpf   	 user.cpf
  json.password   user.password
  json.profile user.profile
  json.organ user.organ
end
