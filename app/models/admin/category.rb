class Admin::Category
  include Mongoid::Document

  has_and_belongs_to_many :services

  field :name, type: String
  field :icon, type: String
  field :active, type: Mongoid::Boolean
  field :permalink, type: String

  before_save :touch_permalink

  def url
    "/categoria/#{self.permalink}"
  end

  private
	  def touch_permalink
	    self.permalink = self.name.parameterize
	  end
end
