var app = angular.module('App', [
																	'uiGmapgoogle-maps',
																	'ngResource', 
																	'ngMessages', 
																	'ngDialog', 
																	'angular-loading-bar', 
																	'ckeditor',
																	'checklist-model',
																	'angularFileUpload'
																]);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
}]);

app.config(function(uiGmapGoogleMapApiProvider) {
  uiGmapGoogleMapApiProvider.configure({
    key: 'AIzaSyDij4Z3AFhetKwsKd9dP6gd0jPceh4Y70I',
    v: '3.20',
    libraries: 'places,geometry,visualization'
  });
})