app.controller('UnitsIndexController', ['$scope', '$http', 'Unit', function($scope, $http, Unit) {
	$scope.units = Unit.query();

	$scope.remove = function(organ) {
		if (confirm('Tem certeza que deseja remover a Unidade de Atendimento?')) {
			var index = $scope.units.indexOf(unit);
			$scope.units.splice(index, 1);
			Unit.delete({ id: unit.id });
		}
	};
}]);

app.controller('UnitsNewController', ['$scope', '$http', 'Unit', 'Organ' , 'User', function($scope, $http, Unit, Organ, User) {
  $scope.unit = {
    schedules: [
      {
        day: 'monday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'tuesday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'wednesday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'thursday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'friday',
        start: '08:00',
        end: '18:00'
      }
    ],
    // coordinates: {
    //   latitude: -9.646375, 
    //   longitude: -35.728146
    // }
  };

  $scope.organs = Organ.query();
  $scope.users  = User.query();

	 $scope.map = {center: {latitude: -9.646375, longitude: -35.728146 }, zoom: 14 };
   $scope.marker = {
    id: Date.now()
  };

$scope.county_places = [
    "Agua Branca","Anadia","Arapiraca","Atalaia","Barra de Santo Antônio","Barra de São Miguel","Batalha","Beleé",
    "Belo Monte","Boca da Mata","Branquinha","Cacimbinhas","Cajueiro","Campestre","Campo Alegre","Campo Grande",
    "Canapi","Capela","Carneiros","Cha Preta","Coite do Nóia","Colonia Leopoldina","Coqueiro Seco","Coruripe",
    "Craibas","Delmiro Gouveia","Dois Riachos","Estrela de Alagoas","Feira Grande","Feliz Deserto","Flexeiras",
    "Girau do Ponciano","Ibateguara","Igaci","Igreja Nova","Inhapi","Jacare dos Homens","Jacuipe","Japaratinga",
    "Jaramataia","Joaquim Gomes","Jequiá da Praia","Jundiá","Junqueiro","Lagoa da Canoa","Limoeiro de Anadia",
    "Maceio","Major Isidoro","Mar Vermelho","Maragogi","Maravilha","Marechal Deodoro","Maribondo","Mata Grande",
    "Matriz de Camaragibe","Messias","Minador do Negrao","Monteiropolis","Murici","Novo Lino","Olho dAgua Grande",
    "Olho dAgua das Flores","Olho dAgua do Casado","Olivenca","Ouro Branco","Palestina","Palmeira dos Indios",
    "Pao de Acucar","Pariconha","Paripueira","Passo de Camaragibe","Paulo Jacinto","Penedo","Piacabucu","Pilar",
    "Pindoba","Piranhas","Poco das Trincheiras","Porto Calvo","Porto Real do Colegio","Porto de Pedras","Quebrangulo",
    "Rio Largo","Roteiro","Santa Luzia do Norte","Santana do Ipanema","Santana do Mundau","Sao Bras","Sao Jose da Laje",
    "Sao Jose da Tapera","Sao Luis do Quitunde","Sao Miguel dos Campos","Sao Miguel dos Milagres","Sao Sebastiao",
    "Satuba","Senador Rui Palmeira","Tanque dArca","Taquarana","Teotônio Vilela","Traipú","Uniao dos Palmares",
    "Vicosa"
  ];

  var events = {
    places_changed: function (searchBox) {
      var place = searchBox.getPlaces()[0];
      
      var lat = place.geometry.location.lat(),lon = place.geometry.location.lng();
      $scope.unit.address= place['formatted_address'];

      $scope.unit.coordinates={
        latitude:lat, 
        longitude: lon
      }
              
      $scope.marker = 
          {
              coordinates: {
                  latitude: lat,
                  longitude: lon,
                  
              }

          };
          $scope.map.center = {
            latitude: lat,
            longitude: lon,            
          };

    }

  }

  $scope.searchbox = { template:'searchbox.tpl.html', position: 'top',events:events}; 
  
	$scope.map_events = {
  	click: function (mapModel, eventName, originalEventArgs) {
  		var e = originalEventArgs[0];
  		$scope.unit.coordinates.latitude = e.latLng.lat();
  		$scope.unit.coordinates.longitude = e.latLng.lng();
  		$scope.$evalAsync();
  	}
  }

  $scope.marker_events = {
  	dragend: function (marker, eventName, args) {
      var lat = marker.getPosition().lat();
      var lon = marker.getPosition().lng();
      $scope.unit.coordinates.latitude = lat;
      $scope.unit.coordinates.longitude = lon;
    }
  }

  $scope.addSchedule = function () {
    if ($scope.unit.schedules == null) {
      $scope.unit.schedules = [];
    }
    $scope.unit.schedules.push({
      day: '',
      start: '08:00',
      end: '18:00'
    });
  };

  $scope.removeSchedule = function (schedule) {
    var index = $scope.unit.schedules.indexOf(schedule);
    $scope.unit.schedules.splice(index, 1);
  };

	$scope.save = function() {
    Unit.save($scope.unit, function() {
      return window.location = '/admin/units';
    });
  };
}]);

app.controller('UnitsEditController', ['$scope', '$http', '$location', 'Unit', 'Organ', 'User', function($scope, $http, $location, Unit, Organ, User) {
	$scope.loadUnit = function (unit_id)  {
    $scope.unit = Unit.get({id: unit_id});
  };

  $scope.organs = Organ.query();
  $scope.users   = User.query();
  
	$scope.map = { center: { latitude: -9.646375, longitude: -35.728146 }, zoom: 13 };

  $scope.county_places = [
    "Agua Branca","Anadia","Arapiraca","Atalaia","Barra de Santo Antonio","Barra de Sao Miguel","Batalha","Belem",
    "Belo Monte","Boca da Mata","Branquinha","Cacimbinhas","Cajueiro","Campestre","Campo Alegre","Campo Grande",
    "Canapi","Capela","Carneiros","Cha Preta","Coite do Noia","Colonia Leopoldina","Coqueiro Seco","Coruripe",
    "Craibas","Delmiro Gouveia","Dois Riachos","Estrela de Alagoas","Feira Grande","Feliz Deserto","Flexeiras",
    "Girau do Ponciano","Ibateguara","Igaci","Igreja Nova","Inhapi","Jacare dos Homens","Jacuipe","Japaratinga",
    "Jaramataia","Joaquim Gomes","Jequiá da Praia","Jundiá","Junqueiro","Lagoa da Canoa","Limoeiro de Anadia",
    "Maceio","Major Isidoro","Mar Vermelho","Maragogi","Maravilha","Marechal Deodoro","Maribondo","Mata Grande",
    "Matriz de Camaragibe","Messias","Minador do Negrao","Monteiropolis","Murici","Novo Lino","Olho dAgua Grande",
    "Olho dAgua das Flores","Olho dAgua do Casado","Olivenca","Ouro Branco","Palestina","Palmeira dos Indios",
    "Pao de Acucar","Pariconha","Paripueira","Passo de Camaragibe","Paulo Jacinto","Penedo","Piacabucu","Pilar",
    "Pindoba","Piranhas","Poco das Trincheiras","Porto Calvo","Porto Real do Colegio","Porto de Pedras","Quebrangulo",
    "Rio Largo","Roteiro","Santa Luzia do Norte","Santana do Ipanema","Santana do Mundau","Sao Bras","Sao Jose da Laje",
    "Sao Jose da Tapera","Sao Luis do Quitunde","Sao Miguel dos Campos","Sao Miguel dos Milagres","Sao Sebastiao",
    "Satuba","Senador Rui Palmeira","Tanque dArca","Taquarana","Teotonio Vilela","Traipu","Uniao dos Palmares",
    "Vicosa"
  ];

  var events = {
    places_changed: function (searchBox) {
      var place = searchBox.getPlaces()[0];
      
      var lat = place.geometry.location.lat(),lon = place.geometry.location.lng();
      $scope.unit.address= place['formatted_address'];

      $scope.unit.coordinates={
        latitude:lat, 
        longitude: lon
      }
              
      $scope.marker = 
          {
              coordinates: {
                  latitude: lat,
                  longitude: lon,
                  
              }

          };
          $scope.map.center = {
            latitude: lat,
            longitude: lon,            
          };

    }

  }

  $scope.searchbox = { template:'searchbox.tpl.html', position: 'top',events:events}; 
  
  
	$scope.map_events = {
  	click: function (mapModel, eventName, originalEventArgs) {
  		var e = originalEventArgs[0];
  		$scope.unit.coordinates.latitude = e.latLng.lat();
  		$scope.unit.coordinates.longitude = e.latLng.lng();
  		$scope.$evalAsync();
  	}
  }

  $scope.marker_events = {
  	dragend: function (marker, eventName, args) {
      var lat = marker.getPosition().lat();
      var lon = marker.getPosition().lng();
      $scope.unit.coordinates.latitude = lat;
      $scope.unit.coordinates.longitude = lon;
    }
  }

  $scope.addSchedule = function () {
    if ($scope.unit.schedules == null) {
      $scope.unit.schedules = [];
    }
    $scope.unit.schedules.push({
      day: '',
      start: '08:00',
      end: '18:00'
    });
  };

  $scope.removeSchedule = function (schedule) {
    var index = $scope.unit.schedules.indexOf(schedule);
    $scope.unit.schedules.splice(index, 1);
  };

	$scope.save = function() {
    Unit.update($scope.unit, function() {
      return window.location = '/admin/units';
    });
  };
}]);
