class Admin::Center
	include Mongoid::Document
	include Mongoid::Pagination

	field :name, type: String
	field :address, type: String
	field :schedules, type: Array
	field :coordinates, type: Hash
	field :image, type: String
	field :permalink, type: String
	field :phones, type: String
  field :email, type: String

	has_and_belongs_to_many :units, class_name: "Admin::Unit"

	before_save :touch_permalink

	def unit_id
  	read_attribute(:unit_id).to_s
	end

	def url
		"/central-de-atendimento/#{permalink}"
	end

  private
    def touch_permalink
      self.permalink = self.name.parameterize
    end
end
