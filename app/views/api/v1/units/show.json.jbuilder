if @show_services == 'true'
	json.organ_id @unit.organ.id.to_s
	json.unit_id @unit._id.to_s
	json.services @unit.services do |service|
		json.service_id service.id.to_s
		json.name service.name
		json.description service.description
		json.categories service.categories do |category|
    	json.id category.id.to_s
    	json.name category.name
    end
	end
end
json.id @unit._id.to_s
json.name @unit.name
json.active @unit.active
json.address @unit.address
json.phones @unit.phones
json.email @unit.email
json.coordinates @unit.coordinates
json.county @unit.county
json.schedules @unit.schedules
json.note_schedules @unit.note_schedules
json.permalink @unit.permalink
json.url @unit.url	
json.service_ids @unit.services.collect {|s|s.id.to_s}
json.organ @unit.organ.id.to_s
json.date @unit.date
json.date_modified @unit.date_modified
