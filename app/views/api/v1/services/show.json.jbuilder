  json.id @service.id.to_s
  json.name @service.name
  json.active @service.active
  json.date @service.date
  json.date_modified @service.date_modified
  json.popular_names @service.popular_names
  json.description @service.description
  json.free @service.free
  json.url @service.url
  json.permalink @service.permalink
  json.steps @service.steps
  json.applicants @service.applicants
  json.other_informations @service.other_informations
  json.estimated_time @service.estimated_time
  json.maturity_level @service.maturity_level
  json.mantener @service.mantener
  json.coordinator @service.coordinator


  json.highlights @service.highlights
  json.highlights_value @service.highlights_value
  json.organ @service.organ.id.to_s
  json.unit_ids @service.units.collect {|u|u.id.to_s}
  json.categories @service.categories do |category|
    json.id category.id.to_s
    json.name category.name
  end
  json.audiences @service.audiences do |audience|
    json.id audience.id.to_s
    json.name audience.name
  end
  json.latest_updater @service.latest_updater
