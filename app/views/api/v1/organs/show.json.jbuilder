if @show_services == 'true'
	json.organ_id @organ.id.to_s
	json.services @organ.services do |service|
		json.id service.id.to_s
		json.name service.name
		json.description service.description
		json.categories service.categories do |category|
    	json.id category.id.to_s
    	json.name category.name
    end
    json.unit_ids service.units.collect {|u|u.id.to_s}
  end
elsif @show_units == 'true'
	json.organ_id @organ.id.to_s
	json.units @organ.units do |unit|
		json.id unit.id.to_s
		json.name unit.name
		json.address unit.address
		json.coordinates unit.coordinates
		json.schedules unit.schedules
		json.phones unit.phones
		json.email unit.email
	end
	json.services @organ.services do |service|
		json.services_id service.id.to_s
	end
else
	json.id @organ.id.to_s
	json.name @organ.name
	json.active @organ.active
	json.county @organ.county
	json.address @organ.address
	json.acronym @organ.acronym	
	json.phones @organ.phones
	json.emails @organ.emails
	json.image @organ.image
	json.nature @organ.nature
	json.coordinates @organ.coordinates
	json.permalink @organ.permalink
	json.url @organ.url
	json.schedules @organ.schedules
	json.office @organ.office 
	json.chief_executive_officer @organ.chief_executive_officer
	json.official_site @organ.official_site
  json.official_email @organ.official_email
	json.popular_names @organ.popular_names
	json.service_ids @organ.services.collect {|s|s.id.to_s}
	json.unit_ids @organ.units.collect {|u|u.id.to_s}
end 
