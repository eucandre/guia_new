json.array!(@units) do |unit|
  json.id unit._id.to_s
  json.name unit.name 
  json.county unit.county
end

