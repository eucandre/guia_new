class	Api::V1::OrgansController < ApplicationController
	def index
		if params[:q]
			@organs = Admin::Organ.full_text_search(params[:q], allow_empty_search: true)
		
		elsif params[:letter]
			@organs = Admin::Organ.where(name: /^#{params[:letter]}/i)
		
		elsif params[:active]
			@organs = Admin::Organ.where(active: params[:active])

		elsif params[:acronym]
			@organs = Admin::Organ.where(acronym: params[:acronym])

		elsif params[:nature]
			@organs = Admin::Organ.where(nature: params[:nature])
			
		else
			@organs = Admin::Organ.order(name: :asc)
		end
	   
	end

	def show
		@organ = Admin::Organ.find(params[:id])
		@organs = Admin::Organ.all
		@show_services = params[:show_services]
		@show_units = params[:show_units]
	end
end
