class Api::UnitsController < ApiController

	def index
		if params[:q]
			@units = Admin::Unit.full_text_search(params[:q], allow_empty_search: true).page(params[:page]).per(50)
		
		elsif params[:organ_id]
			@units =  Admin::Unit.where(organ_id: params[:organ_id]).page(params[:page]).per(50)
		
		elsif params[:letter]
			@units =  Admin::Unit.where(name: /^#{params[:letter]}/i).page(params[:page]).per(50)
			
		elsif params[:county]
			@units =  Admin::Unit.where(county: params[:county])

		else
			@units = Admin::Unit.where(active: true).page(params[:page]).per(50)
			
		end
	end

	def show
		@unit = Admin::Unit.find(params[:id])
	end

	def by_organ

	end

	def by_user

	end

end
