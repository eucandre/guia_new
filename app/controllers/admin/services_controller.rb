class Admin::ServicesController < AdminController
	before_action :set_service, only: [:show, :edit, :update, :destroy]

	before_action :verify_permission

	skip_before_filter :verify_authenticity_token


	def index
		if current_user.admin?
			@services = Service.all		
			#@services = Service.search(params[:search]).order(name).paginate(:per_page => 5, :page => params[:page])

		else
			@services = Service.where(organ_id: current_user.organ_id)
		end
	end

	def new
		inspect
		@service = Service.new
		if current_user
			register_user = File.new("register.txt","a")
			register_user.puts("***********")
			register_user.puts("usuário ",current_user.name)
			register_user.puts("id do perfil ",current_user.profile_id)
			register_user.puts("Seu perfil no sistema",current_user.profile.name)
			register_user.puts("Logado em ", current_user.current_sign_in_at)
			register_user.puts("id do órgão ", current_user.organ_id)
			register_user.puts("Nome do órgão ", current_user.organ.name)
			register_user.puts('Criou serviço id ', @service.id.to_s, "\n")
			register_user.puts("Criou serviço em ", Time.now.strftime("%d/%m/%Y %H:%M"))
			register_user.puts("***********")
			register_user.puts("\n")
			register_user.close()
		end
		@service.latest_updater = current_user.name
	end

	def show
		
	end

	def edit
		@service.date_modified = Time.now.strftime("%d/%m/%Y %H:%M")
		@service.latest_updater = current_user.name
	end

	def create
		@service = Service.new(service_params)
		@service.organ_id = current_user.organ_id if !current_user.admin?
		@service.date = Time.now.strftime("%d/%m/%Y %H:%M")

		respond_to do |format|
			if @service.save
				format.html { redirect_to services_url, notice: 'Serviço criado com sucesso criada com sucesso.' }
				format.json { render :show, status: :created, location: [:admin, @service] }
			else
				format.html { render :new }
				format.json { render json: @service.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		
		@service.date_modified = Time.now.strftime("%d/%m/%Y %H:%M")
		@service.latest_updater = current_user.name
		
		respond_to do |format|
			if @service.update(service_params)
				format.html { redirect_to "/admin/services", notice: 'Serviço atualizado com sucesso.' }
				format.json { render :show, status: :ok, location: [:admin, @service] }
			else
				format.html { render :edit }
				format.json { render json: @services.error, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@service.destroy
		respond_to do |format|
			format.html { redirect_to "/admin/services", notice: 'Serviço excluido com sucesso.' }
			format.json { head :no_content }
		end
	end

	private
	def set_service
		
		@service = Service.find(params[:id])

	end

	def service_params
		params.permit(
						:name,
						:date,
						:date_modified,
						:active,
						:highlights,
						:highlights_value,
						:mantener,
						:coordinator,
						:latest_updater,
						:maturity_level,
						:acronym,
						:description,
						:free,
						:permalink,
						:organ_id,
						:other_informations,
						 estimated_time: [
								:type,
								:unit,
								:min,
								:max,
								:description,
							],
							unit_ids: [],
							audience_ids: [],
							category_ids: [],
							popular_names: [:name],
							applicants: [:type, :requirements],
							steps: [
								:title, :description,
								documents: [:name, cases: [:description]],
								providing_channels: [:type, :description],
								cost: [:description, :coin, :value, :type],
								type_delivery_channel: [:type]
							]
)
	end
end
