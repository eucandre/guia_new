json.array!(@admin_profiles) do |admin_profile|
  json.id admin_profile._id.to_s
  json.name admin_profile.name
end
