app.controller('UsersIndexController', ['$scope', '$http', 'User', function($scope, $http, User) {
	 $scope.users = User.query();
}]);


app.controller('UsersNewController', ['$scope', '$http', '$routeParams', '$location', 'User', function($scope, $http, $routeParams, $location, User) {
	$scope.user = {};

	$scope.save = function() {
		User.save($scope.user);
		return $location.path('/users');
	};
}]);

app.controller('UsersEditController', ['$scope', '$http', '$location', 'User', 'FileUploader', function($scope, $http, $location, User, FileUploader) {
   	$scope.loadOrgan = function (user_id) {
        $scope.user = User.get({id: user_id});
      };
    
     var uploader = $scope.uploader = new FileUploader({
        url: '/admin/users/uploader'
      });
    }