class	ServicesController < ApplicationController

	def index
    @organs = Admin::Organ.where(active: true).order(name: :asc)
	end

	def show
		@service =  Service.where(id: params[:id]).first
    @service_units = @service.units.all

		set_meta_tags title:       @service.name,
                  description: @service.description,
                  keywords:    @service.get_keys,
                  author:      'Secretaria de Estado do Planejamento, Gestão e Patrimônio',
                  og: {
                    title:       @service.name,
                    description: @service.description,
                    type:        'website',
                    url:         @service.url,
                    image:       @service.organ.attachment
                  }
	end

end
