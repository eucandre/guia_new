json.array!(@centers) do |center|
  json.id center._id.to_s
  json.name center.name
  json.units center.units do |unit|
  	json.id unit.id.to_s
  	json.name unit.name
  	json.organ_id unit.organ_id
  end
end
