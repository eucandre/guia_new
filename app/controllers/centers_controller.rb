class	CentersController < ApplicationController

	def index
		@centers = Admin::Center.where(active: params[:true]).all
	end

	def show
		@center = Admin::Center.where(permalink: params[:permalink]).first
	end

end
