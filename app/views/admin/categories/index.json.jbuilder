json.array!(@admin_categories) do |admin_category|
  json.id admin_category._id.to_s
  json.name admin_category.name
  json.active admin_category.active
end
