class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception

  before_action do 
  	@categories = Admin::Category.where(active: true).order(name: :asc)
  	@audiences = Admin::Audience.where(active: true).order(name: :asc)

    set_meta_tags title:       'Guia de Serviços do estado de Alagoas',
                  description: 'O projeto tem como intuito facilitar o acesso a informações sobre os serviços de governo mais utilizados pela população, além de funcionar como um catálogo de consulta, com dados sobre taxas, documentos necessários, modelo de prestação (online ou presencial) e endereços dos órgãos responsáveis pelos serviços.',
                  keywords:    ['Serviços', 'órgãos','informações', 'guia', 'atendimento', 'pagamento'],
                  author:      'Secretaria de Estado do Planejamento, Gestão e Patrimônio',
                  og: {
                    title:       'Guia de Serviços do estado de Alagoas',
                    description: 'O projeto tem como intuito facilitar o acesso a informações sobre os serviços de governo mais utilizados pela população, além de funcionar como um catálogo de consulta, com dados sobre taxas, documentos necessários, modelo de prestação (online ou presencial) e endereços dos órgãos responsáveis pelos serviços.',
                    type:        'website',
                    url:         'http://www.servicos.al.gov.br/',
                    image:       'http://servicos.al.gov.br/assets/layout/brasao.png',
                  }
  end

  def cached(cache_name, value)
    Rails.cache.fetch("#{CACHE_PREFIX}/#{cache_name}", expires_in: MODEL_CACHE_TIME) { value }
  end
end
