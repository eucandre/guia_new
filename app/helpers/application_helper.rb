module ApplicationHelper
	def can_view(controller, action = 'index')
		x = current_user.profile.auth_actions.collect { |a| { controller: a.auth_controller.name, action: a.name } }

		by_pass = false

		x.each do |y|
		  if y[:controller] == controller.to_s && y[:action] == action.to_s
		    by_pass = true
		    break
		  end
		end

		return by_pass
	end
end
