class ApiController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  protect_from_forgery with: :exception

  def cached(cache_name, value)
    Rails.cache.fetch("#{CACHE_PREFIX}/#{cache_name}", expires_in: MODEL_CACHE_TIME) { value }
  end
end
