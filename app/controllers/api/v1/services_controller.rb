class	Api::V1::ServicesController < ApplicationController
	def index
		if params[:q]
			@services = Service.full_text_search(params[:q], allow_empty_search: true)
		
		elsif params[:organ_id]
			@services = Service.where(organ_id: params[:organ_id])
		
		elsif params[:letter]
			@services = Service.where(name: /^#{params[:letter]}/i)
		
		elsif params[:active]
			@services = Service.where(active: params[:active])

		else
			@services = Service.order(name: :asc)			
		end	
		
	end

	def show
		@service = Service.find(params[:id])
	end
end