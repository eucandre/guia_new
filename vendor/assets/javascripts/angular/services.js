app.factory('Service', function($resource) {
  return $resource('/admin/services/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});

app.factory('Organ', function($resource) {
  return $resource('/admin/organs/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});

app.factory('Category', function($resource) {
  return $resource('/admin/categories/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});

app.factory('Unit', function($resource) {
  return $resource('/admin/units/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});

app.factory('Center', function($resource) {
  return $resource('/admin/centers/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});

app.factory('Audience', function($resource) {
  return $resource('/admin/audiences/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});

app.factory('User', function($resource) {
  return $resource('/admin/users/:id.json', { id: '@id' }, { update: { method: 'PUT' } } );
});
