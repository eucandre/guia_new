json.id @service.id.to_s
json.name @service.name
json.acronym @service.acronym
json.popular_names @service.popular_names
json.description @service.description
json.free @service.free
json.applicants @service.applicants
json.estimated_time @service.estimated_time
json.steps @service.steps
json.permalink @service.permalink
json.other_informations @service.other_informations
json.active @service.active
json.date @service.date
json.highlights @service.highlights
json.highlights_value @service.highlights_value
json.mantener @service.mantener
json.coordinator @service.coordinator
json.maturity_level @service.maturity_level
json.latest_updater @service.latest_updater

# associations
json.organ_id     @service.organ.id.to_s if @service.organ
json.unit_ids     @service.units.map { |u| u.id.to_s }
json.audience_ids @service.audiences.map { |a| a.id.to_s	}
json.category_ids @service.categories.map {	|c| c.id.to_s	}