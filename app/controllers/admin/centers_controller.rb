class Admin::CentersController < AdminController
  before_action :set_admin_center, only: [:show, :edit, :update, :destroy]

  before_action :verify_permission

  skip_before_filter :verify_authenticity_token

  def index
    @admin_centers = Admin::Center.order(name: :dsc)
  end

  def new
    @admin_center = Admin::Center.new
  end

  def edit

  end

  def show
    render json: @admin_center
  end

  def create
    @admin_center = Admin::Center.new(admin_center_params)

    respond_to do |format|
      if @admin_center.save
        format.html { redirect_to admin_centers_url, notice: 'Centro de atendimento criado com sucesso.' }
        format.json { render :show, status: :created, location: @admin_center }
      else
        format.html { render :new }
        format.json { render json: @admin_center.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_center.update(admin_center_params)
        format.html { redirect_to admin_centers_url, notice: 'Centro de atendimento atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @admin_center }
      else
        format.html { render :edit }
        format.json { render json: @admin_center.errors, status: :unprocessable_entity }
      end
    end
  end

 def destroy
    @admin_center.destroy
    respond_to do |format|
      format.html { redirect_to admin_centers_url, notice: 'Centro de atendimento excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_center
      @admin_center = Admin::Center.find(params[:id])
    end

    def admin_center_params
      params.permit(
                      :name,
                      :address,
                      :image,
                      :phones,
                      :email,
                      schedules: [:day, :start, :end],
                      coordinates: [:latitude, :longitude],
                      unit_ids: []
                    )
    end
end
