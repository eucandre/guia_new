class Service
	include Mongoid::Document
	include Mongoid::Search
	include Mongoid::Pagination
	

	field :name, type: String
	field :acronym, type: String
	field :popular_names, type: Array
	field :description, type: String
	field :free, type: Boolean
	field :applicants, type: Array
	field :estimated_time, type: Hash
	field :steps, type: Array
	field :permalink, type: String
	field :other_informations, type: String
	field :active, type: Boolean
	field :highlights, type: Boolean
	field :highlights_value, type: String
	field :date, type: String
	field :date_modified, type: String
	field :maturity_level, type: String
	field :mantener, type: String
	field :coordinator, type: String
	field :latest_updater, type: String

	belongs_to :organ, class_name: "Admin::Organ"
	has_and_belongs_to_many :units, class_name: "Admin::Unit"
	has_and_belongs_to_many :audiences, class_name: "Admin::Audience"
	has_and_belongs_to_many :categories, class_name: "Admin::Category"

	before_save :touch_permalink
	
	
	search_in :name, #:popular_names, categories: :name, organ: :name





	def url
		"#{SITE_URL}/servico/#{id}/#{permalink}"
	end

	def get_keys
		return nil if self.popular_names.blank?

		keys = []

		self.popular_names.collect { |p| keys << p[:name] }

		keys = keys.join(', ')
	end

	private
	
	  def touch_permalink
	    self.permalink = self.name.parameterize
	  end
end		
