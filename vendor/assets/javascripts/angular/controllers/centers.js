app.controller('CentersIndexController', ['$scope', '$http', 'Center', function($scope, $http, Center) {
	$scope.centers = Center.query();

	$scope.remove = function(organ) {
		if (confirm('Tem certeza que deseja remover a Central de Atendimento?')) {
			var index = $scope.centers.indexOf(center);
			$scope.centers.splice(index, 1);
			Center.delete({ id: center.id });
		}
	};
}]);

app.controller('CentersNewController', ['$scope', '$http', 'Center', 'Unit', function($scope, $http, Center, Unit) {
  $scope.center = {
    schedules: [
      {
        day: 'monday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'tuesday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'wednesday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'thursday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'friday',
        start: '08:00',
        end: '18:00'
      }
    ],
    coordinates: {
      latitude: -9.646375, 
      longitude: -35.728146
    }
  };

  $scope.units = Unit.query();

	$scope.map = { center: { latitude: -9.646375, longitude: -35.728146 }, zoom: 13 };

	$scope.map_events = {
  	click: function (mapModel, eventName, originalEventArgs) {
  		var e = originalEventArgs[0];
  		$scope.center.coordinates.latitude = e.latLng.lat();
  		$scope.center.coordinates.longitude = e.latLng.lng();
  		$scope.$evalAsync();
  	}
  }

  $scope.marker_events = {
  	dragend: function (marker, eventName, args) {
      var lat = marker.getPosition().lat();
      var lon = marker.getPosition().lng();
      $scope.cente.coordinates.latitude = lat;
      $scope.center.coordinates.longitude = lon;
    }
  }

  $scope.addSchedule = function () {
    if ($scope.center.schedules == null) {
      $scope.center.schedules = [];
    }
    $scope.center.schedules.push({
      day: '',
      start: '08:00',
      end: '18:00'
    });
  };

  $scope.removeSchedule = function (schedule) {
    var index = $scope.center.schedules.indexOf(schedule);
    $scope.center.schedules.splice(index, 1);
  };

	$scope.save = function() {
    Center.save($scope.center, function() {
      return window.location = '/admin/centers';
    });
  };
}]);

app.controller('CentersEditController', ['$scope', '$http', '$location', 'Center', 'Unit', function($scope, $http, $location, Center, Unit) {
	$scope.loadCenter = function (center_id)  {
    $scope.center = Center.get({id: center_id});
  };

  $scope.units = Unit.query();

	$scope.map = { center: { latitude: -9.646375, longitude: -35.728146 }, zoom: 13 };

	$scope.map_events = {
  	click: function (mapModel, eventName, originalEventArgs) {
  		var e = originalEventArgs[0];
  		$scope.center.coordinates.latitude = e.latLng.lat();
  		$scope.center.coordinates.longitude = e.latLng.lng();
  		$scope.$evalAsync();
  	}
  }

  $scope.marker_events = {
  	dragend: function (marker, eventName, args) {
      var lat = marker.getPosition().lat();
      var lon = marker.getPosition().lng();
      $scope.center.coordinates.latitude = lat;
      $scope.center.coordinates.longitude = lon;
    }
  }

  $scope.addUnit = function (unit) {
    if ($scope.center.unit_ids.indexOf(unit.id) == -1) {
      $scope.center.unit_ids.push(unit.id);
    } else {
      var index = $scope.center.unit_ids.indexOf(unit.id);
      $scope.center.unit_ids.splice(index, 1);
    }
  };

  $scope.addSchedule = function () {
    if ($scope.center.schedules == null) {
      $scope.center.schedules = [];
    }
    $scope.center.schedules.push({
      day: '',
      start: '08:00',
      end: '18:00'
    });
  };

  $scope.removeSchedule = function (schedule) {
    var index = $scope.center.schedules.indexOf(schedule);
    $scope.center.schedules.splice(index, 1);
  };

	$scope.save = function() {
    Center.update($scope.center, function() {
      return window.location = '/admin/centers';
    });
  };
}]);
