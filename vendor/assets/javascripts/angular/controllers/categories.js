app.controller('CategoriesIndexController', ['$scope', '$http', 'Category', function($scope, $http, Category) {
	$scope.categories = Category.query();

	// $scope.remove = function(organ) {
	// 	if (confirm('Tem certeza que deseja remover o orgão?')) {
	// 		var index = $scope.organs.indexOf(organ);
	// 		$scope.organs.splice(index, 1);
	// 		Organ.delete({ id: organ.id });
	// 	}
	// };
}]);

app.controller('CategoriesNewController', ['$scope', '$http', '$routeParams', '$location', 'Category', function($scope, $http, $routeParams, $location, Category) {
	$scope.category = {};

	$scope.save = function() {
		Category.save($scope.category);
		return $location.path('/categories');
	};
}]);

app.controller('CategoriesEditController', ['$scope', '$http', '$routeParams', '$location', 'Category', function($scope, $http, $routeParams, $location, Category) {
	$scope.organ = Organ.get({id: $routeParams.id});

	$scope.save = function() {
		Category.update($scope.category);
		return $location.path('/categories');
	};
}]);
