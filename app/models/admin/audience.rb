class Admin::Audience
  include Mongoid::Document

	has_and_belongs_to_many :services

  field :name, type: String
  field :active, type: Mongoid::Boolean
  field :icon, type: String
  field :permalink, type: String

  before_save :touch_permalink

  def service_id
  	read_attribute(:service_id).to_s
  end

  def url
    "/publico-alvo/#{self.permalink}"
  end

  private
	  def touch_permalink
	    self.permalink = self.name.parameterize
	  end
end
