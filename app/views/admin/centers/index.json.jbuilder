json.array!(@admin_centers) do |admin_center|
  json.id admin_unit._id.to_s
  json.name admin_center.name
  json.address admin_center.address
  json.schedules admin_center.schedules
  json.image admin_center.image
  json.permalink admin_center.permalink
  json.url admin_center.url
end