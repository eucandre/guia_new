class Api::UsersController < ApiController

	def index
		if params[:q]
			@users = User.full_text_search(params[:q], allow_empty_search: true)
		elsif params[:organ_id]
			@users =  User.where(organ_id: params[:organ_id])
		elsif params[:letter]
			@users = User.where(name: /^#{params[:letter]}/i)
		else
			@users =  User.where(active: true).page(params[:page])
		end
	end

	def show
		@user = User.find(params[:id])
	end

	def by_organ

	end

	def by_user

	end

end
