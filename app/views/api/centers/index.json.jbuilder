json.array!(@centers) do |center|
  json.id center._id.to_s
  json.name center.name
  json.address center.address
  json.phones center.phones
  json.email center.email
  json.schedules center.schedules
  json.image center.image
  json.permalink center.permalink
  json.url center.url
end
