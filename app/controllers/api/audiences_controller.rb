class Api::AudiencesController < ApiController

	def index
		if params[:q]
			@audiences = Admin::Audience.full_text_search(params[:q], allow_empty_search: true)
		elsif params[:letter]
			@audiences =  Admin::Audience.where(name: /^#{params[:letter]}/i)
		else
			@audiences =  Admin::Audience.all
		end
	end

end
