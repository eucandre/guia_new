json.units @units.each do |unit|
  json.id unit._id.to_s
  json.name unit.name
  json.active unit.active
  json.address unit.address
  json.phones unit.phones
  json.email unit.email
  json.schedules unit.schedules
  json.note_schedules unit.note_schedules
  json.image unit.image
  json.permalink unit.permalink
  json.url unit.url
  json.county unit.county
  json.organ do 
  	json.id unit.organ.id.to_s
    json.name unit.organ.name
    json.address unit.organ.address
    json.acronym unit.organ.acronym
    json.phones unit.organ.phones
    json.emails unit.organ.emails
    json.schedules unit.organ.schedules
    json.image unit.organ.image
    json.permalink unit.organ.permalink
  end if unit.organ
  json.user do 
    json.id unit.user.id.to_s
    json.name unit.user.name
    json.profile unit.user.profile
  end if unit.user 
  json.services unit.services 
end
json.total_pages @units.total_pages
