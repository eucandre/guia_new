app.controller('OrgansIndexController', ['$scope', '$http', 'Organ', function($scope, $http, Organ) {
	 $scope.organs = Organ.query();
}]);

app.controller('OrgansNewController', ['$scope', '$http', '$location', 'Organ', function($scope, $http, $location, Organ) {
	$scope.organ = {
    schedules: [
      {
        day: 'monday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'tuesday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'wednesday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'thursday',
        start: '08:00',
        end: '18:00'
      },
      {
        day: 'friday',
        start: '08:00',
        end: '18:00'
      }
    ],
		// coordinates: {
		// 	latitude: -9.646375, 
		// 	longitude: -35.728146
		// }
	};

  $scope.map = {center: {latitude: -9.646375, longitude: -35.728146 }, zoom: 14 };
  $scope.marker = {
    id: Date.now()
  };

  $scope.county_places = [
    "Agua Branca","Anadia","Arapiraca","Atalaia","Barra de Santo Antonio","Barra de Sao Miguel","Batalha","Belem",
    "Belo Monte","Boca da Mata","Branquinha","Cacimbinhas","Cajueiro","Campestre","Campo Alegre","Campo Grande",
    "Canapi","Capela","Carneiros","Cha Preta","Coite do Noia","Colonia Leopoldina","Coqueiro Seco","Coruripe",
    "Craibas","Delmiro Gouveia","Dois Riachos","Estrela de Alagoas","Feira Grande","Feliz Deserto","Flexeiras",
    "Girau do Ponciano","Ibateguara","Igaci","Igreja Nova","Inhapi","Jacare dos Homens","Jacuipe","Japaratinga",
    "Jaramataia","Joaquim Gomes","Jequiá da Praia","Jundia","Junqueiro","Lagoa da Canoa","Limoeiro de Anadia",
    "Maceio","Major Isidoro","Mar Vermelho","Maragogi","Maravilha","Marechal Deodoro","Maribondo","Mata Grande",
    "Matriz de Camaragibe","Messias","Minador do Negrao","Monteiropolis","Murici","Novo Lino","Olho dAgua Grande",
    "Olho dAgua das Flores","Olho dAgua do Casado","Olivenca","Ouro Branco","Palestina","Palmeira dos Indios",
    "Pao de Acucar","Pariconha","Paripueira","Passo de Camaragibe","Paulo Jacinto","Penedo","Piacabucu","Pilar",
    "Pindoba","Piranhas","Poco das Trincheiras","Porto Calvo","Porto Real do Colegio","Porto de Pedras","Quebrangulo",
    "Rio Largo","Roteiro","Santa Luzia do Norte","Santana do Ipanema","Santana do Mundau","Sao Bras","Sao Jose da Laje",
    "Sao Jose da Tapera","Sao Luis do Quitunde","Sao Miguel dos Campos","Sao Miguel dos Milagres","Sao Sebastiao",
    "Satuba","Senador Rui Palmeira","Tanque dArca","Taquarana","Teotonio Vilela","Traipu","Uniao dos Palmares",
    "Vicosa"
  ];
    
  var events = {
    places_changed: function (searchBox) {
      var place = searchBox.getPlaces()[0];
      
      var lat = place.geometry.location.lat(),lon = place.geometry.location.lng();
      $scope.organ.address= place['formatted_address'];

      $scope.organ.coordinates={
        latitude:lat, 
        longitude: lon
      }
              
      $scope.marker = 
          {
              coordinates: {
                  latitude: lat,
                  longitude: lon,
                  
              }

          };
          $scope.map.center = {
            latitude: lat,
            longitude: lon,            
          };

    }

  }
  
 $scope.searchbox = { template:'searchbox.tpl.html', position: 'top',events:events}; 
         
  

	$scope.map_events = {
  	click: function (mapModel, eventName, originalEventArgs) {
  		var e = originalEventArgs[0];
  		$scope.organ.coordinates.latitude = e.latLng.lat();
  		$scope.organ.coordinates.longitude = e.latLng.lng();
  		$scope.$evalAsync();
  	}
  }

  $scope.marker_events = {
  	dragend: function (marker, eventName, args) {
      var lat = marker.getPosition().lat();
      var lon = marker.getPosition().lng();
      $scope.organ.coordinates.latitude = lat;
      $scope.organ.coordinates.longitude = lon;
    }
  }

  $scope.addSchedule = function() {
    if ($scope.organ.schedules == null) {
      $scope.organ.schedules = [];
    }
    $scope.organ.schedules.push({
      day: '',
      start: '08:00',
      end: '18:00'
    });
  };

  $scope.removeSchedule = function(schedule) {
    var index = $scope.organ.schedules.indexOf(schedule);
    $scope.organ.schedules.splice(index, 1);
  };

  $scope.addPopularName = function() {
    if ($scope.organ.popular_names == undefined) {
      $scope.organ.popular_names = [];
    }

    $scope.organ.popular_names.push({});
  };

  $scope.removePopularName = function (popular_name) {
    var index = $scope.organ.popular_names.indexOf(popular_name);
    $scope.organ.popular_names.splice(index, 1);
  };

	$scope.save = function() {
    Organ.save($scope.organ, function() {
      return window.location = '/admin/organs';
    });
	};
}]);

app.controller('OrgansEditController', ['$scope', '$http', '$location', 'Organ', 'FileUploader', function($scope, $http, $location, Organ, FileUploader) {
	$scope.loadOrgan = function (organ_id) {
    $scope.organ = Organ.get({id: organ_id});
  };

  var uploader = $scope.uploader = new FileUploader({
    url: '/admin/organs/uploader'
  });

  uploader.onBeforeUploadItem = function(item) {
    item.formData.push({organ_id: $scope.organ.id});
  };

	
$scope.map = {center: {latitude: -9.646375, longitude: -35.728146 }, zoom: 14 };
  $scope.marker = {
    id: Date.now()
  };

  $scope.county_places = [
    "Agua Branca","Anadia","Arapiraca","Atalaia","Barra de Santo Antonio","Barra de Sao Miguel","Batalha","Belem",
    "Belo Monte","Boca da Mata","Branquinha","Cacimbinhas","Cajueiro","Campestre","Campo Alegre","Campo Grande",
    "Canapi","Capela","Carneiros","Cha Preta","Coite do Noia","Colonia Leopoldina","Coqueiro Seco","Coruripe",
    "Craibas","Delmiro Gouveia","Dois Riachos","Estrela de Alagoas","Feira Grande","Feliz Deserto","Flexeiras",
    "Girau do Ponciano","Ibateguara","Igaci","Igreja Nova","Inhapi","Jacare dos Homens","Jacuipe","Japaratinga",
    "Jaramataia","Joaquim Gomes","Jequiá da Praia","Jundia,Junqueiro","Lagoa da Canoa","Limoeiro de Anadia",
    "Maceio","Major Isidoro","Mar Vermelho","Maragogi","Maravilha","Marechal Deodoro","Maribondo","Mata Grande",
    "Matriz de Camaragibe","Messias","Minador do Negrao","Monteiropolis","Murici","Novo Lino","Olho dAgua Grande",
    "Olho dAgua das Flores","Olho dAgua do Casado","Olivenca","Ouro Branco","Palestina","Palmeira dos Indios",
    "Pao de Acucar","Pariconha","Paripueira","Passo de Camaragibe","Paulo Jacinto","Penedo","Piacabucu","Pilar",
    "Pindoba","Piranhas","Poco das Trincheiras","Porto Calvo","Porto Real do Colegio","Porto de Pedras","Quebrangulo",
    "Rio Largo","Roteiro","Santa Luzia do Norte","Santana do Ipanema","Santana do Mundau","Sao Bras","Sao Jose da Laje",
    "Sao Jose da Tapera","Sao Luis do Quitunde","Sao Miguel dos Campos","Sao Miguel dos Milagres","Sao Sebastiao",
    "Satuba","Senador Rui Palmeira","Tanque dArca","Taquarana","Teotonio Vilela","Traipu","Uniao dos Palmares",
    "Vicosa"
  ];
    
  var events = {
    places_changed: function (searchBox) {
      var place = searchBox.getPlaces()[0];
      
      var lat = place.geometry.location.lat(),lon = place.geometry.location.lng();
      $scope.organ.address= place['formatted_address'];

      $scope.organ.coordinates={
        latitude:lat, 
        longitude: lon
      }
              
      $scope.marker = 
          {
              coordinates: {
                  latitude: lat,
                  longitude: lon,
                  
              }

          };
          $scope.map.center = {
            latitude: lat,
            longitude: lon,            
          };

    }

  }
$scope.searchbox = { template:'searchbox.tpl.html', position: 'top', events:events}; 


$scope.map_events = {

    click: function (mapModel, eventName, originalEventArgs) {
      var e = originalEventArgs[0];
      $scope.organ.coordinates.latitude = e.latLng.lat();
      $scope.organ.coordinates.longitude = e.latLng.lng();
      $scope.$evalAsync();
    }
  }

  $scope.marker_events = {
    dragend: function (marker, eventName, args) {
      var lat = marker.getPosition().lat();
      var lon = marker.getPosition().lng();
      $scope.organ.coordinates.latitude = lat;
      $scope.organ.coordinates.longitude = lon;
    }
  }
  
  $scope.addSchedule = function() {
    if ($scope.organ.schedules == null) {
      $scope.organ.schedules = [];
    }
    $scope.organ.schedules.push({
      day: '',
      start: '08:00',
      end: '18:00'
    });
  };

  $scope.removeSchedule = function(schedule) {
    var index = $scope.organ.schedules.indexOf(schedule);
    $scope.organ.schedules.splice(index, 1);
  };

  $scope.addPopularName = function() {
 
    if ($scope.organ.popular_names == undefined) {
      $scope.organ.popular_names = [];
    }

    $scope.organ.popular_names.push({});
  };

  $scope.removePopularName = function (popular_name) {
    var index = $scope.organ.popular_names.indexOf(popular_name);
    $scope.organ.popular_names.splice(index, 1);
  };

	$scope.save = function() {
    $scope.uploader.uploadAll();
		Organ.update($scope.organ, function() {
      return window.location = '/admin/organs';
    });
	};
}]);
