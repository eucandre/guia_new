json.array!(@organs) do |organ|
  json.id organ.id.to_s
  json.name organ.name
  json.active organ.active
  json.acronym organ.acronym
  json.nature organ.nature
end
