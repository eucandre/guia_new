class Admin::UnitsController < AdminController
  before_action :set_admin_unit, only: [:show, :edit, :update, :destroy]

  before_action :verify_permission, if: -> { request.path_parameters[:action] == 'index' && !request.format.json? }

  skip_before_filter :verify_authenticity_token

  def index
    if current_user.admin?
      @admin_units = Admin::Unit.all
    else
      @admin_units = Admin::Unit.where(organ_id: current_user.organ_id)
    end

  end

  def new
    @admin_unit = Admin::Unit.new
  end

  def edit
    @admin_unit.date = Time.now.strftime("%d/%m/%Y %H:%M")
  end

  def show
    render json: @admin_unit
  end

  def create

    @admin_unit = Admin::Unit.new(admin_unit_params)
    @admin_unit.organ_id = current_user.organ_id if !current_user.admin?
    @admin_unit.date = Time.now.strftime("%d/%m/%Y %H:%M")
    respond_to do |format|
      if @admin_unit.save
        format.html { redirect_to admin_unit_url, notice: 'Unidade de atendimento criada com sucesso.' }
        format.json { render :show, status: :created, location: @admin_unit }
      else
        format.html { render :new }
        format.json { render json: @admin_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @admin_unit.date_modified = Time.now.strftime("%d/%m/%Y %H:%M")
      if @admin_unit.update(admin_unit_params)
        format.html { redirect_to admin_unit_url, notice: 'Unidade de atendimento atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @admin_unit }
      else
        format.html { render :edit }
        format.json { render json: @admin_unit.errors, status: :unprocessable_entity }
      end
    end
  end

 def destroy
    @admin_unit.destroy
    respond_to do |format|
      format.html { redirect_to admin_units_url, notice: 'Unidade de atendimento excluida com sucesso.' }
      format.json { head :no_content }
      end
  end

  private
    def set_admin_unit
      @admin_unit = Admin::Unit.find(params[:id])
    end

    def admin_unit_params
      params.permit(
                      :name,
                      :active,
                      :address,
                      :phones,
                      :email,
                      :image,
                      :organ_id,
                      :user_id,
                      :county,
                      :note_schedules,
                      schedules: [:day, :start, :end],
                      coordinates: [:latitude, :longitude]
                    )
    end
end
