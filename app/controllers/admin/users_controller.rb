class Admin::UsersController < AdminController

  before_action :set_user, only: [:show, :edit, :update, :destroy]

	before_action :verify_permission

  def index
    @users = User.all
  end

  def new
    @user     = User.new
    @profiles = Admin::Profile.all
    @organs   = Admin::Organ.all
  end

  def edit
    @profiles = Admin::Profile.all
    @organs   = Admin::Organ.all
  end

  def uploader
    user = User.find(params[:user_id])
    user.attachment = params[:file]

    render json: user.save
  end

  def show
     @user = User.find(params[:user_id])
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_users_url, notice: 'Usuário criado com sucesso.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

    if user_params[:password].blank?
     @user.update_without_password(user_params_without_password)
    else
     @user.update(user_params)
    end

    if @user.valid?
      if @user == current_user
        sing_in(@user, baypass: true)
        end
        redirect_to admin_users_url
      else
        render action: 'edit'
      end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'Usuário Alvo excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params_without_password
      user_params.delete(:password)
      user_params.delete(:password_confirmation)
      user_params
    end

    def user_params
       params.require(:user).permit(
                                      :name,
                                      :email,
                                      :cpf,
                                      :password,
                                      :password_confirmation,
                                      :attachment,
                                      :profile_id,
                                      :organ_id,
                                    )
    end
end
