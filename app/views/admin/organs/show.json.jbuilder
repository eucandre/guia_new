json.extract! @admin_organ, :id.to_s, :name, :active, :address, :acronym, :phones, :emails, :nature, :schedules, :popular_names, :image, :county, :report_url
