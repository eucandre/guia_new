class CategoriesController < ApplicationController

	def index
		@category = Admin::Category.where(permalink: params[:permalink]).first
		@services = @category.services.page(params[:page]).per(15).order(name: :asc)
	end

	def show
		@categories = Admin::Category.order(name: :asc)
	end

end
