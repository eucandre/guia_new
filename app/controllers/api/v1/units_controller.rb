class Api::V1::UnitsController < ApiController
	def index
		if params[:q]
			@units = Admin::Unit.full_text_search(params[:q], allow_empty_search: true)
		
		elsif params[:organ_id]
			@units = Admin::Unit.where(organ_id: params[:organ_id])
		
		elsif params[:letter]
			@units = Admin::Unit.where(name: /^#{params[:letter]}/i)
		
		elsif params[:county]
			@units = Admin::Unit.where(county: params[:county])
		
		else
			@units = Admin::Unit.order(name: :asc)
			@organs = Admin::Organ.all
			@user = User.order(name: :asc)
		end	
	end

	def show
		@unit = Admin::Unit.find(params[:id])
		@show_services = params[:show_services]
	end

	def by_organ
		
	end

	def by_user
		
	end
end
