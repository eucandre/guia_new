class AudiencesController < ApplicationController

	def index
		@audience = Admin::Audience.where(permalink: params[:permalink]).first
		@services = @audience.services.page(params[:page]).per(15)
	end

	def show
		@audiences = Admin::Audience.where(permalink: params[:permalink]).first
	end

end
