app.controller('ServicesIndexController', ['$scope', '$http', 'Service', function ($scope, $http, Service) {
	$scope.services = Service.query();

	$scope.remove = function (service) {
		if (confirm('Tem certeza que deseja remover o serviço?')) {
			var index = $scope.services.indexOf(service);
			$scope.services.splice(index, 1);
			Service.delete({ id: service.id });
		}
	};
}]);

app.controller('ServicesNewController', ['$scope', '$http', '$location', 'Service', 'Organ', 'Unit','Audience','Category','User', function($scope, $http, $location, Service, Organ, Unit, Audience, Category, User) {
	$scope.service = {};
	$scope.organs = Organ.query();
	$scope.units = Unit.query();
	$scope.audiences = Audience.query();
	$scope.categories = Category.query();
	$scope.user = User.query();

	$scope.save = function() {
    Service.save($scope.service, function() {
      return window.location = '/admin/services';
    });
  };

	$scope.addApplicant = function() {
		if ($scope.service.applicants == null) {
			$scope.service.applicants = [];
		}
		$scope.service.applicants.push({
			type: "",
			requirements: ""
		});
	};

	$scope.removeApplicant = function (step) {
		var index = $scope.service.applicants.indexOf(step);
		$scope.service.applicants.splice(index, 1);
	};

	$scope.addStep = function() {
		if ($scope.service.steps == null) {
			$scope.service.steps = [];
		}
		$scope.service.steps.push({
			title: "",
			description: "",
			cost:{coin:"R$"}
		});
	};

	$scope.removeStep = function (step) {
		var index = $scope.service.steps.indexOf(step);
		$scope.service.steps.splice(index, 1);
	};

	$scope.addDocument = function (step) {
		var index = $scope.service.steps.indexOf(step);

		if ($scope.service.steps[index].documents == undefined) {
			$scope.service.steps[index].documents = [];
		};

		$scope.service.steps[index].documents.push({});

	};

	$scope.removeDocument = function (step, document) {
		var step_index = $scope.service.steps.indexOf(step);
		var document_index = $scope.service.steps[step_index].documents.indexOf(document);
		$scope.service.steps[step_index].documents.splice(document_index, 1);
	};

	$scope.addChannel = function (step) {
		var index = $scope.service.steps.indexOf(step);
		$scope.somePlaceholder;
		$scope.tipo;
		if ($scope.service.steps[index].providing_channels == undefined) {
			$scope.service.steps[index].providing_channels = [];
		};
		$scope.service.steps[index].providing_channels.push({});
	  $scope.somePlaceholder = $scope.service.steps[index].providing_channels[0];
	};
	$scope.removeChannel = function (step, providing_channel) {
		var step_index = $scope.service.steps.indexOf(step);
		var providing_channel_index = $scope.service.steps[step_index].providing_channels.indexOf(providing_channel);
		$scope.service.steps[step_index].providing_channels.splice(providing_channel_index, 1);
	};

	$scope.addCase = function (step, document) {
		var step_index = $scope.service.steps.indexOf(step);
		var document_index = $scope.service.steps[step_index].documents.indexOf(document);

		if ($scope.service.steps[step_index].documents[document_index].cases == undefined) {
			$scope.service.steps[step_index].documents[document_index].cases = [];
		};

		$scope.service.steps[step_index].documents[document_index].cases.push({});
	};

	$scope.removeCase = function (step, document, casex) {
		var step_index = $scope.service.steps.indexOf(step);
		var document_index = $scope.service.steps[step_index].documents.indexOf(document);
		var case_index = $scope.service.steps[step_index].documents[document_index].cases.indexOf(casex);

		$scope.service.steps[step_index].documents[document_index].cases.splice(case_index, 1);
	};

	$scope.addPopularName = function() {
		if ($scope.service.popular_names == undefined) {
			$scope.service.popular_names = [];
		}

		$scope.service.popular_names.push({});
	};

	$scope.removePopularName = function (popular_name) {
		var index = $scope.service.popular_names.indexOf(popular_name);
		$scope.service.popular_names.splice(index, 1);
	};

}]);

app.controller('ServicesEditController', ['$scope', '$http', '$location', 'Service', 'Organ', 'Unit','Audience','Category', function($scope, $http, $location, Service, Organ, Unit, Audience, Category) {

	$scope.loadService = function(service_id) {
		$scope.service = Service.get({id: service_id});
	};

	$scope.organs = Organ.query();
	$scope.units = Unit.query();
	$scope.audiences = Audience.query();
	$scope.categories = Category.query();

	$scope.addUnit = function (unit) {
		if ($scope.service.unit_ids.indexOf(unit.id) == -1) {
		  $scope.service.unit_ids.push(unit.id);
		} else {
			var index = $scope.service.unit_ids.indexOf(unit.id);
			$scope.service.unit_ids.splice(index, 1);
		}
	};

	$scope.addCategory = function (category) {
		if ($scope.service.category_ids.indexOf(category.id) == -1) {
		  $scope.service.category_ids.push(category.id);
		} else {
			var index = $scope.service.category_ids.indexOf(category.id);
			$scope.service.category_ids.splice(index, 1);
		}
	};

	$scope.addAudience = function (audience) {
		if ($scope.service.audience_ids.indexOf(audience.id) == -1) {
		  $scope.service.audience_ids.push(audience.id);
		} else {
			var index = $scope.service.audience_ids.indexOf(audience.id);
			$scope.service.audience_ids.splice(index, 1);
		}
	};

	$scope.addApplicant = function() {
		if ($scope.service.applicants == null) {
			$scope.service.applicants = [];
		}
		$scope.service.applicants.push({
			type: "",
			requirements: "",
		});
	};

	$scope.removeApplicant = function (step) {
		var index = $scope.service.applicants.indexOf(step);
		$scope.service.applicants.splice(index, 1);
	};

	$scope.addStep = function() {
		if ($scope.service.steps == null) {
			$scope.service.steps = [];
		}
		$scope.service.steps.push({
			title: "",
			description: "",
		});
	};

	$scope.removeStep = function (step) {
		var index = $scope.service.steps.indexOf(step);
		$scope.service.steps.splice(index, 1);
	};

	$scope.addDocument = function (step) {
		var index = $scope.service.steps.indexOf(step);

		if ($scope.service.steps[index].documents == undefined) {
			$scope.service.steps[index].documents = [];
		};

		$scope.service.steps[index].documents.push({});
	};

	$scope.removeDocument = function (step, document) {
		var step_index = $scope.service.steps.indexOf(step);
		var document_index = $scope.service.steps[step_index].documents.indexOf(document);
		$scope.service.steps[step_index].documents.splice(document_index, 1);
	};

	$scope.addChannel = function (step) {
		var index = $scope.service.steps.indexOf(step);

		if ($scope.service.steps[index].providing_channels == undefined) {
			$scope.service.steps[index].providing_channels = [];
		};

		$scope.service.steps[index].providing_channels.push({});
	};

	$scope.removeChannel = function (step, providing_channel) {
		var step_index = $scope.service.steps.indexOf(step);
		var providing_channel_index = $scope.service.steps[step_index].providing_channels.indexOf(providing_channel);
		$scope.service.steps[step_index].providing_channels.splice(providing_channel_index, 1);
	};

	$scope.addCase = function (step, document) {
		var step_index = $scope.service.steps.indexOf(step);
		var document_index = $scope.service.steps[step_index].documents.indexOf(document);

		if ($scope.service.steps[step_index].documents[document_index].cases == undefined) {
			$scope.service.steps[step_index].documents[document_index].cases = [];
		};

		$scope.service.steps[step_index].documents[document_index].cases.push({});
	};

	$scope.removeCase = function (step, document, casex) {
		var step_index = $scope.service.steps.indexOf(step);
		var document_index = $scope.service.steps[step_index].documents.indexOf(document);
		var case_index = $scope.service.steps[step_index].documents[document_index].cases.indexOf(casex);

		$scope.service.steps[step_index].documents[document_index].cases.splice(case_index, 1);
	};

	$scope.addPopularName = function() {
		if ($scope.service.popular_names == undefined) {
			$scope.service.popular_names = [];
		}

		$scope.service.popular_names.push({});
	};

	$scope.removePopularName = function (popular_name) {
		var index = $scope.service.popular_names.indexOf(popular_name);
		$scope.service.popular_names.splice(index, 1);
	};

	$scope.save = function() {
    Service.update($scope.service, function() {
      return window.location = '/admin/services';
    });
  };

}]);
